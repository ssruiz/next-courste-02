import { Pokemon, Sprites } from '../interfaces';

interface PokemonInfo {
  id: number;
  name: string;
  sprites: Sprites[];
}

export const getFewDataPokemon = (pokemon: Pokemon) => ({
  id: pokemon.id,
  name: pokemon.name,
  sprites: pokemon.sprites,
});
