export { default as FavoriteCardPokemon } from './FavoriteCardPokemon';
export { default as FavoritesPokemon } from './FavoritesPokemon';
export * from './PokemonCard';
export * from './PokemonDetail';
