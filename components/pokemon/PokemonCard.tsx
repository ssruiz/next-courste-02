import { FC } from 'react';
import {
  Button,
  Card,
  Container,
  Divider,
  Grid,
  Row,
  Text,
} from '@nextui-org/react';

import { SmallPokemon } from '../../interfaces';
import { useRouter } from 'next/router';

interface Props {
  pokemon: SmallPokemon;
}
export const PokemonCard: FC<Props> = ({ pokemon: { id, img, name } }) => {
  const router = useRouter();
  const onClick = () => {
    router.push(`/name/${name}`);
  };
  return (
    <Grid xs={6} sm={3} md={2} xl={1} key={id}>
      <Card onClick={onClick} color={'primary'} hoverable clickable>
        <Card.Header>
          <Text transform="capitalize">{name}</Text>
        </Card.Header>
        <Divider />
        <Card.Body css={{ p: 1 }}>
          <Card.Image objectFit="fill" src={img} width="100%" height={100} />
        </Card.Body>
        <Card.Footer>
          <Row justify="space-between">
            <Text>#{id}</Text>
          </Row>
        </Card.Footer>
      </Card>
    </Grid>
  );
};
