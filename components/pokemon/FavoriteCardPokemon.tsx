import { Card } from '@nextui-org/react';
import { useRouter } from 'next/router';
import React, { FC } from 'react';

interface Props {
  pokemon: number;
}

const FavoriteCardPokemon: FC<Props> = ({ pokemon }) => {
  const router = useRouter();

  const handleClick = (id: number) => {
    router.push(`/pokemon/${id}`);
  };

  return (
    <Card
      hoverable
      clickable
      css={{ padding: 10 }}
      onClick={() => handleClick(pokemon)}
    >
      <Card.Image
        src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${pokemon}.svg`}
        width={'100%'}
        height={150}
      />
    </Card>
  );
};

export default FavoriteCardPokemon;
