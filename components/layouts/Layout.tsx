import Head from 'next/head';
import { FC } from 'react';
import { Navbar } from '../ui';
import { useRouter } from 'next/router';

interface Props {
  title?: string;
}

const origin = typeof window === 'undefined' ? '' : window.location.origin;

export const Layout: FC<Props> = ({ children, title = 'Pokemon App' }) => {
  return (
    <>
      <Head>
        <title>{title}</title>
        <meta name="author" content="Samuel Ruiz" />
        <meta
          name="description"
          content={`"Información sobre el pokemon ${title}`}
        />
        <meta name="keywords" content={`${title}, pokemon, pokedex`} />
        <meta property="og:title" content={`Info Pokemon sobre ${title}`} />
        <meta
          property="og:description"
          content={`Curso NextJS - Información sobre ${title} `}
        />
        <meta
          property="og:image"
          itemProp="image"
          content={`${origin}/img/banner2.png`}
        />
      </Head>
      {/* NAVBAR */}
      <Navbar />
      <main style={{ padding: '0px 20px' }}>{children}</main>
    </>
  );
};
