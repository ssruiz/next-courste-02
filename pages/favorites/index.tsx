import { Card, Container, Grid, Image, Text } from '@nextui-org/react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { Layout } from '../../components/layouts';
import { FavoritesPokemon } from '../../components/pokemon';
import { NoFavorites } from '../../components/ui/';
import localfavorites from '../../utils/localfavorites';

const FavoritesPage: NextPage = () => {
  const [listadoFavorites, setListadoFavorites] = useState<number[]>([]);

  useEffect(() => {
    setListadoFavorites(localfavorites.pokemons());
  }, []);

  return (
    <Layout>
      {listadoFavorites.length === 0 ? (
        <NoFavorites />
      ) : (
        <FavoritesPokemon pokemons={listadoFavorites} />
      )}
    </Layout>
  );
};

export default FavoritesPage;
