import { GetStaticPaths, GetStaticProps, NextPage } from 'next';

import { Button, Card, Container, Grid, Image, Text } from '@nextui-org/react';
import { PokeAPi } from '../../api';
import { Layout } from '../../components/layouts';
import { PokemonCard, PokemonDetail } from '../../components/pokemon';
import { Pokemon, PokemonResponse, SmallPokemon } from '../../interfaces';
import { getFewDataPokemon } from '../../utils';

interface Props {
  pokemon: Pokemon;
}

const PokemonByNamePage: NextPage<Props> = ({ pokemon }) => {
  return <PokemonDetail pokemon={pokemon} />;
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const { name } = params as { name: string };

  const { data } = await PokeAPi.get<Pokemon>(`pokemon/${name}`);

  return {
    props: {
      pokemon: getFewDataPokemon(data),
    },
  };
};

export const getStaticPaths: GetStaticPaths = async (ctx) => {
  const { data } = await PokeAPi.get<PokemonResponse>('/pokemon?limit=150');
  return {
    paths: data.results.map((pokemon) => ({ params: { name: pokemon.name } })),
    fallback: false,
  };
};

export default PokemonByNamePage;
